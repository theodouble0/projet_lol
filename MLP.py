from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import matplotlib.pyplot as plt
import sklearn.datasets
import sklearn.ensemble
import csv
import os
import time
import numpy as np
import pandas as pd
import pickle
import sys
from sklearn.preprocessing import StandardScaler

import IPython

import lime
import lime.lime_tabular as lt

# PECQUEUX Théo
# HUYGHES Antoine


def numberSlash(mot):
    compteur = 0
    for lettre in mot:
        if lettre == '/':
            compteur = compteur + 1
    return(compteur)

def label_encode(data):
    for x in data:
        if x[0] == "Tstart":
            x[0] = 0
        elif x[0] == "Tearly":
            x[0] = 1
        elif x[0] == "Tmid":
            x[0] = 2
        elif x[0] == "Tend":
            x[0] = 3

def save_model(model):
    pickle.dump(model, open("mlp_save/" + save_name + ".sav", 'wb'))

def load_model():
    return pickle.load(open("mlp_save/" + save_name + ".sav", 'rb'))

def create_mlp(x_train, y_train):
    print("############################## CREATE and FIT ##############################\n")
    tps1 = time.time()
    mlp = MLPClassifier(activation="relu", hidden_layer_sizes=(10,10,10,10), max_iter=1000, alpha=0.1, learning_rate_init = 0.01, tol = 0.01, shuffle=True)
    mlp.fit(x_train, y_train)    
    tps2 = time.time()
    print("temps de création du MLP : " + str(tps2 - tps1))
    if save:
        save_model(mlp)
    return mlp

def print_score(classifier, x_train, y_train, x_test, y_test):    
    print("############################## TEST ##############################\n")
    #print(x_train, y_train[i])
    print("\u001B[4m\nTrain\u001B[0m : \u001B[32m" , classifier.score(x_train, y_train) , "\u001B[0m")
    print("\u001B[4mTest\u001B[0m : \u001B[32m" , classifier.score(x_test, y_test) , "\u001B[0m")

def load_all_game():
    print("############################## load all games ##############################\n")
    joueur = 0
    data = []
    for ELO in division:
        for NUMBER in elo:
            print("     -------------------- " + ELO + "_" + NUMBER + " --------------------\n")
            for root, dirs, files in os.walk("data/" + ELO + "_" + NUMBER):
                if numberSlash(root) == 3:
                    joueur = joueur + 1
                    load_game(root, data)
    return data

def manage_items(d, items, col_buy, col1, col2):
    ignore = ['', '2003', '2055', '1400', '1401', '1402', '1412', '1413', '1414', '1416', '1419', '3340', '3363', '3364'] # 2003->potion, 2055->pink        enchantement_item_jungler(1400, 1401, 1402, 1412, 1413, 1414, 1416, 1419)
    unique = []
    rm = []
    for i in d[col_buy].strip('][').split(', '): #Convert a string representation of list into list
        if i not in ignore:
            if i in unique:
                if i in items:
                    continue
            items.append(i)
    index = 0
    for i in d[col2].strip('][').split(', '): #Convert a string representation of list into list
        i = i.replace("]", "")
        i = i.replace("[", "")
        if i!="":
            if index%2 == 0:
                if i != "0" and i not in ignore:
                    if i in unique:
                        if i in items:
                            continue
                    items.append(i)
            else:
                try:
                    rm.append(i)
                except :
                    pass        
        index += 1
    for i in d[col1].strip('][').split(', '): #Convert a string representation of list into list
        if i not in ignore:
            try:
                rm.append(i)
            except :
                pass
                
    for i in rm:
        try:
            items.remove(i)
        except:
            pass
    
    if len(items) >6:
        print("\u001B[31merror items oversized : " , items , "\u001B[0m")
        raise Exception("items oversized")

def load_game(game, games_x):
    start = len(games_x)
    try:
        data_timeline = pd.read_csv(game + "/timeline.csv")
        data_game = pd.read_csv(game + "/game.csv")
    except:
        print("\u001B[31mErreur dans l'ouverture d'un fichier\u001B[0m")
        return [], [-1, -1]
    del(data_timeline['name'])
    x = data_timeline.transpose()[1:].values.tolist()
    
    # ----- Gestion des items ------
    items_0 = []
    items_1 = []
    items_2 = []
    items_3 = []
    items_4 = []
    items_5 = []
    items_6 = []
    items_7 = []
    items_8 = []
    items_9 = []
    items= [items_0, items_1, items_2,items_3, items_4, items_5,items_6, items_7, items_8, items_9]
    for d in x: # for each frame
        #print("\nstart : " + str(d))
        try:
            manage_items(d, items[0], 7, 8, 9)
            manage_items(d, items[1], 15, 16, 17)
            manage_items(d, items[2], 23, 24, 25)
            manage_items(d, items[3], 31, 32, 33)
            manage_items(d, items[4], 39, 40, 41)
            manage_items(d, items[5], 47, 48, 49)
            manage_items(d, items[6], 55, 56, 57)
            manage_items(d, items[7], 63, 64, 65)
            manage_items(d, items[8], 71, 72, 73)
            manage_items(d, items[9], 79, 80, 81)
        except:
            #print("\u001B[31merreur dans les items\u001B[0m")
            pass
            return [], [-1, -1]
            
            
        #for items0 in items:
        #    print(items0)
            
        for player in items:
            index = 0
            for item in player:
                d.append(item)
                index += 1
            while(index <6 ):
                d.append("-1")
                index += 1
        
        #print("\nbefire : " + str(d))
        for remove in [8-1,16-1-3,24-1-6, 32-1-9, 40-1-12, 48-1-15, 56-1-18, 64-1-21,72-1-24,80-1-27]:
            #print(remove)
            del d[remove]
            del d[remove]
            del d[remove]
        
        #del d[8-1:10-1]#:16-1:17-1:18-1:24-1:25-1:26-1: 32-1:33-1:34-1: 40-1:41-1:42-1: 48-1:49-1:50-1: 56-1:57-1:58-1: 64-1:65-1:66-1:72-1:73-1:74-1:80-1:81-1:82-1]
        #print("after : " + str(d))
    
    #data_timeline = data_timeline.drop([8-1, 9-1,10-1,16-1,17-1,18-1,24-1,25-1,26-1, 32-1,33-1,34-1, 40-1,41-1,42-1, 48-1,49-1,50-1, 56-1,57-1,58-1, 64-1,65-1,66-1,72-1,73-1,74-1,80-1,81-1,82-1])
    #x = x.transpose()[1:].values.tolist()

    # -------------------------------
        
        
    data_timeline = data_timeline.reset_index()

    # -------------------- test du tableau de l'article - partie timeline.csv --------------------
    # datas -> lignes des datas -> résultats(train, test)

    # kill par joueurs -> [6, 14, 22, 30, 38, 46, 54, 62, 70, 78] -> 0.901, 0.902 
    # kill par équipe -> [82, 87] -> 0.901, 0.901 
    # kill delta -> [92] -> 0.902, 0.907

    # currentGold par joueurs -> [3, 11, 19, 27, 35, 43, 51, 59, 67, 75] -> 0.913, 0.898
    # currentGold par équipe -> [83,88] -> 0.901, 0.898
    # currentGold delta -> [93] -> 0.901; 0.901

    # totalGold par joueurs -> [2, 10, 18, 26, 34, 42, 50, 58, 66, 74] -> 0.962, 0.959
    # totalGold par équipe -> [84,89] -> 0.962, 0.960
    # totalGold delta -> [94] -> 0.961, 0.961

    # levels par joueurs -> [4, 12, 20, 28, 36, 44, 52, 60, 68, 76] -> 0.922, 0.923
    # levels par équipe -> [85,90] -> 0.891, 0.920
    # levels delta -> [95] -> 0.915, 0.923 

    # sbires par joueurs -> [5, 13, 21, 29, 37, 45, 53, 61, 69, 77] -> 0.629, 0.612 
    # sbires par équipe -> [86,91] -> 0.620, 0.623
    # sbires delta -> [96] -> 0.633, 0.631
    # data_timeline = data_timeline.reset_index()
    # data_timeline = data_timeline.iloc[[5, 13, 21, 29, 37, 45, 53, 61, 69, 77]] 
    # data_timeline = data_timeline.iloc[:, -1]
    # print(data_timeline)
    # x = [data_timeline.values.tolist()]

    # -------------------- test du tableau de l'article - partie game.csv ---------------------
    # building_kills -> [7,8] -> int(x[0][1])+int(x[1][1]), int(x[0][2])+int(x[1][2]) -> 0.947, 0.946
    # building_kills delta ->  [7,8] -> int(x[0][1])+int(x[1][1]) - int(x[0][2])+int(x[1][2]) -> 0.926, 0.932

    # building_deaths ->  [7,8] -> int(x[0][2])+int(x[1][2]), int(x[0][1])+int(x[1][1]) -> 0.942, 0.948
    # building_deaths delta ->  [7,8] -> int(x[0][2])+int(x[1][2]) - int(x[0][1])+int(x[1][1]) -> 0.933, 0.933

    # tower_kills -> [7] -> [[int(x[0][1]), int(x[0][2])]] -> 0.948, 0.948
    # tower_kills delta -> [7] -> [[int(x[0][1]) - int(x[0][2])]] -> 0.948, 0.944 
    
    # tower_deaths -> [7] -> [[int(x[0][2]), int(x[0][1])]] -> 0.947, 0.949 
    # tower_deaths delta -> [7] ->  [[int(x[0][2]) - int(x[0][1])]] -> 0.948, 0.945
    # x = data_game
    # x = x.iloc[[7, 8]].values.tolist()
    # x = [[ int(x[0][2])+int(x[1][2]) - int(x[0][1])+int(x[1][1]) ]]
    #print("\n\n")
    #print(" ",x[0:10],"\n" )
    # x= x[0:15]
    # calcul Y
    if data_game["team0"][0] == "Win":
        for test in x:
            test.append(1) #[1,0]
    else:
        for test in x:
            test.append(0) #[0,1]
    # print(x)
    games_x += x

    if len(games_x) <= 100:
        print("game : " + game + "[" + str(start) + "," + str(len(games_x)) + "]")


def split(data):
    train, test = train_test_split(data, test_size = 1/3)
    x_train = []
    y_train = []
    for tr in train:
        y_train.append(tr.pop())
        x_train.append(tr)
    x_test = []
    y_test = []
    for te in test:
        y_test.append(te.pop())
        x_test.append(te)
    x_train = np.array(x_train).astype(np.float64).tolist()
    x_test = np.array(x_test).astype(np.float64).tolist()

    nb = 0
    for i in x_test[10] :
       print( str(nb) + " : " + str(i) )
       nb += 1
    
    ss = StandardScaler()
    x_train = ss.fit_transform(x_train)
    x_test = ss.fit_transform(x_test)
    return x_train, y_train, x_test, y_test


def prob(data):
    return np.array(list(zip(1-model.predict(data),model.predict(data))))

def lime(mlp, x_test, y_test):
    print("#################### LIME ####################")
    #print(x_test[0])
    explainer = lt.LimeTabularExplainer(x_test, mode='classification', feature_names=None, categorical_features=[], verbose=True)
    #print(x_test)
    model = mlp
    for i in [10]:
        print()
        print(str(i) , " " , model.predict(x_test[i].reshape(1,-1)))
        #print(x_test[i])
        tps1 = time.time()
        #print(x_test[i])
        print()
        exp = explainer.explain_instance(x_test[i], prob, num_features=127, num_samples=5000) #default num_sampl = 5000
        print(exp.as_list())
        print(exp.as_pyplot_figure())
        plt.show()
        plt.savefig("lime.png")
        tps2 = time.time()
        print("temps de explain_instance : " + str(tps2 - tps1))
        


def main_test():
    data = load_all_game()
    label_encode(data)
    x_train, y_train, x_test, y_test = split(data)
    mlp = load_model()
    print_score(mlp, x_train, y_train, x_test, y_test)
    print("FIN")


def main():
    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
    data = load_all_game()
    label_encode(data)
    x_train, y_train, x_test, y_test = split(data)
    mlp = create_mlp(x_train, y_train)
    print_score(mlp, x_train, y_train, x_test, y_test)
    global model
    model = mlp
    lime(mlp, x_test, y_test)
    print("FIN")


save = False
division = ["IRON", "BRONZE","SILVER", "GOLD","PLATINUM", "DIAMOND"]
elo = ["IV"]#, "III", "II", "I"]
if len(sys.argv) == 1:
    main()
elif len(sys.argv) == 2 and sys.argv[1] == "--save":
    print("preciser le nom de la sauvegarde")
    exit()
elif len(sys.argv) == 3 and sys.argv[1] == "--save":
    save = True
    save_name = sys.argv[2]
    main()
elif len(sys.argv) == 2 and sys.argv[1] == "--test":
    print("preciser le nom du mlp a utiliser")
    exit()
elif len(sys.argv) == 3 and sys.argv[1] == "--test":
    save_name = sys.argv[2]
    main_test()
elif len(sys.argv) > 3 and sys.argv[1] == "--test":
    save_name = sys.argv[2]
    division = []
    for i in range(3, len(sys.argv)):
        division.append(sys.argv[i])
    main_test()
elif len(sys.argv) > 2 and sys.argv[1] == "--elo":
    division = []
    for i in range(2, len(sys.argv)):
        division.append(sys.argv[i])
    main()
