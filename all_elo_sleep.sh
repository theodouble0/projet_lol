#!/bin/bash

#24 ELO
test=$(cat nb_requete.txt)
for ELO in "IRON" "BRONZE" "SILVER" "GOLD" "PLATINUM" "DIAMOND"
do
    for DIV in "IV" "III" "II" "I" 
    do
        echo "---------- run  -> $ELO $DIV ----------"
        python3 extract.py $1 $2 $ELO $DIV $test
        echo "---------- end  -> $ELO $DIV ----------"
        if ([ $ELO != "DIAMOND" ] && [ $DIV != "I" ])
        then 
            echo "SLEEP"
            sleep 120
        fi
    done
    if [ $ELO != "DIAMOND" ]
    then 
        echo "SLEEP"
        sleep 120
    fi
done