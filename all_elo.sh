#!/bin/bash

#24 ELO

echo "1" > nb_requete.txt
#"IRON" "BRONZE" "SILVER"   
for ELO in "GOLD" "PLATINUM" "DIAMOND"
do
    for DIV in "III" "II" "I" "IV" 
    do
        test=$(cat nb_requete.txt)
        echo "---------- run  -> $ELO $DIV ----------"
        python3 extract.py $1 $2 $ELO $DIV $test
        echo "---------- end  -> $ELO $DIV ----------"
    done
done