# PROJET (HUYGHES Antoine, PECQUEUX Théo)

--- 
## How to extract : 

* The ELO available for the extraction :
  * IRON -> I, II, III, IV
  * BRONZE -> I, II, III, IV
  * SILVER -> I, II, III, IV
  * GOLD -> I, II, III, IV
  * PLATINUM -> I, II, III, IV
  * DIAMOND -> I, II, III, IV



* If you want to extract 1 game per player, for 1000 players : 
```bash
./all_elo.sh 1 1000 
````

* If you want to extract 1000 games per player, for 1 player 
```bash
./all_elo.sh 1000 1
```

___

## How to execute MLP:

```bash
python3 MLP.py
```
* execution options:
  * `--save name` -> save mlp as mlp_save/name.sav
  * `--elo DIAMOND GOLD` -> launches the mlp on the specified elos (IRON, BRONZE, SILVER, GOLD, PLATINUM, DIAMOND)
  * `--test name` -> run the MLP name.sav on all elo
  * `--test name DIAMOND SILVER` -> run the MLP name.sav on the elo specify

