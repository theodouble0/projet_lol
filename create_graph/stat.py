import csv, os
import matplotlib.pyplot as plt
import numpy as np


def numberSlash(mot):
    compteur = 0
    for lettre in mot:
        if lettre == '/':
            compteur = compteur + 1
    return(compteur)

index = 0
x_label = []
somme = []
nb = []
for ELO in ["IRON", "BRONZE", "SILVER", "GOLD", "PLATINUM", "DIAMOND"] :
    for NUMBER in ["IV", "III", "II", "I"] :
        x_label.append(NUMBER)
        nb.append(0)
        somme.append(0)
        print("---------- " + ELO + "_" + NUMBER + "----------")
        for root, dirs, files in os.walk("data/" + ELO + "_" + NUMBER):
            teams1Win = False
            teams1FirstBlood = False
            if numberSlash(root) == 3:
                with open(root + "/game.csv", newline='') as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                    for row in spamreader:
                        if row[0] == "firstTower":
                            if row[1] == "True":
                                teams1Win = True
                        elif row[0] == "firstInhibitor":
                            if row[1] > row[2]:
                                teams1FirstBlood = True
                if (teams1FirstBlood and teams1Win) or (not teams1Win and not teams1FirstBlood):
                    somme[index] = somme [index] +1 
                nb[index] = nb[index] +1
        index = index +1

per = []
for i in range(len(somme)):
    per.append(somme[i]/nb[i]*100)
    print( str(somme[i]) + " / " + str(nb[i]) + " = " + str(per[i]) )

fig, ax = plt.subplots(1,1)
for i in range(len(nb)):
    if i < 4:
        color = "grey"
    elif i < 8:
        color = "orange"
    elif i < 12:
        color = "silver"
    elif i < 16:
        color = "gold"
    elif i < 20:
        color = "aquamarine"
    else :
        color = "aqua"
    plt.bar(i + 0.5, height = per[i] , color = color,edgecolor = "black", width=1)
        
x_plot = np.linspace(1,24,24)
y_plot = []
plt.xlim(0,24)
plt.ylim(0,100)
ax.set_xticks(np.arange(0,24,1))
ax.set_xticklabels(x_label, fontsize = 11)
plt.yticks([0,10,20,30,40,50,60,70,80,90,100])
plt.title('First inhibitor if first tower')
plt.xlabel('ELO')
plt.ylabel('% of games')
plt.show()
