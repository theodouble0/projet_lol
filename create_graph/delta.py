import csv, os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import PercentFormatter

def numberSlash(mot):
    compteur = 0
    for lettre in mot:
        if lettre == '/':
            compteur = compteur + 1
    return(compteur)

somme = []
nb = []
index = -1
x_label = []

for ELO in ["IRON", "BRONZE", "SILVER", "GOLD", "PLATINUM", "DIAMOND"] :
    for NUMBER in ["IV", "III", "II", "I"] :
        x_label.append(NUMBER)
        nb.append(0)
        somme.append(0)
        index = index+1
        print("---------- " + ELO + "_" + NUMBER + "----------")
        for root, dirs, files in os.walk("data/" + ELO + "_" + NUMBER):
            if numberSlash(root) == 3:
                win = ""
                gold = ""
                nb[index] = nb[index] + 1
                try:
                    with open(root + "/timeline.csv", newline='') as csvfile:
                        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                        for row in spamreader:
                            if row[0] == "delta_TOTALGOLD":
                                if int(row[len(row)-1]) > 0:
                                    gold = "team0"
                                else:
                                    gold = "team1"
                except:
                    print("file doesn't exist->timeline")
                
                try:
                    with open(root + "/game.csv", newline='') as csvfile:
                        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                        for row in spamreader:
                            if row[0] == "win":
                                if row[1] == "Win":
                                    win = "team0"
                                else:
                                    win = "team1"
                                    break
                except:
                    print("file doesn't exist->game")
                
                if win == gold:
                    somme[index] = somme[index] + 1

per = []
for i in range(len(somme)):
    #print(str(somme[i]) + " " + str(nb[i]))
    per.append(somme[i]/nb[i]*100)
    print( per[i] )


fig, ax = plt.subplots(1,1)
for i in range(len(per)):
    if i < 4:
        color = "grey"
    elif i < 8:
        color = "orange"
    elif i < 12:
        color = "silver"
    elif i < 16:
        color = "gold"
    elif i < 20:
        color = "aquamarine"
    else :
        color = "aqua"
    plt.bar(i + 0.5, height = per[i] , color = color,edgecolor = "black", width=1)

plt.title("TITLE")
plt.xlabel('ELO')
plt.ylabel('percentage of (team win + gold>)')
plt.xlim(0,24)
plt.ylim(85,95)
plt.xticks([2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,8])
plt.yticks([85, 87.5 ,90, 92.5, 95,100])

ax.set_xticks(np.arange(0,24,1))
ax.set_xticklabels(x_label, fontsize = 11)
plt.show()



#["IRON I","IRON II", "IRON III", "IRON IV", "BRONZE I","BRONZE II", "BRONZE III", "BRONZE IV", "SILVER I","SILVER II", "SILVER III", "SILVER IV", "GOLD I", "GOLD II", "GOLD III", "GOLD IV", "PLATINIUM I", "PLATINIUM II", "PLATINIUM III", "PLATINIUM IV", "DIAMOND I", "DIAMOND II", "DIAMOND III", "DIAMOND IV"]