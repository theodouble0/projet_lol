import csv, os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import PercentFormatter

def numberSlash(mot):
    compteur = 0
    for lettre in mot:
        if lettre == '/':
            compteur = compteur + 1
    return(compteur)

data = []
index = 0
nb = 0
for ELO in ["IRON", "BRONZE", "SILVER", "GOLD", "PLATINUM", "DIAMOND"] :
    for NUMBER in ["IV", "III", "II", "I"] :
        print("---------- " + ELO + "_" + NUMBER + "----------")
        for root, dirs, files in os.walk("data/" + ELO + "_" + NUMBER):
            nb = nb+1
            if numberSlash(root) == 3:
                try:
                    with open(root + "/timeline.csv", newline='') as csvfile:
                        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                        for row in spamreader:
                            if row[0] == "nameStamp":
                                for i in range(len(row)):
                                    if row[i] == "Tmid":
                                        data.append(i/len(row)*100)
                                        break
                except:
                    print("file doesn't exist")


print(data)
plt.title("first baron killed")
plt.xlabel('% of match duration (p)')
plt.ylabel('# of matches')
plt.hist(data , weights = np.ones(len(data))/len(data),bins = range(10,100, 5))
plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
plt.show()