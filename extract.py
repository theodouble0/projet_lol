import requests, json, sys, time, csv, os
import pandas as pd
#urllib, array
##### PARAM #####
KEY = "RGAPI-23f09ead-482d-4621-8b57-44b5f4198232"
ELO = ["SILVER", "I"]
nb_requete = 1

if len(sys.argv) == 2:
    history_size = int(sys.argv[1])
    nb_player = 10
elif len(sys.argv) == 3:
    history_size = int(sys.argv[1])
    nb_player = int(sys.argv[2])
elif len(sys.argv) == 4:
    print("please enter the DIVISION(I,II,III,IV)")
    exit()
elif len(sys.argv) == 5:
    history_size = int(sys.argv[1])
    nb_player = int(sys.argv[2])
    ELO[0] = sys.argv[3]
    ELO[1] = sys.argv[4]
elif len(sys.argv) == 6:
    history_size = int(sys.argv[1])
    nb_player = int(sys.argv[2])
    ELO[0] = sys.argv[3]
    ELO[1] = sys.argv[4]
    nb_requete = int(sys.argv[5])
    print("nb_requete -> " + str(nb_requete))
else:
    history_size = 10
    nb_player = 10
    

def requete(url):
    max_try = 5
    essaie = 0
    error = -1
    global nb_requete
    while error != 200 and essaie < max_try:
        res = requests.get(url)
        nb_requete = nb_requete + 1
        error = res.status_code
        if nb_requete >= 97:
            print("----- COOLDOWN start -----")
            time.sleep(100)
            print("----- COOLDOWN end -----")
            nb_requete = 0
        elif error != 200 :
            essaie = essaie + 1
            time.sleep(1)
    file_object = open("nb_requete.txt", "w")   
    file_object.write(str(nb_requete))
    file_object.close()
    if error == 200:
        return res
    elif error == 404:
        print("erreur dans la requéte (" + url + ") -> ERREUR_CODE: " + str(error))
        return res
    else:
        print("erreur dans la requéte (" + url + ") -> ERREUR_CODE: " + str(error) + ", fin du programme")
        exit()

nb_page = int(nb_player/205 +1 )

summonerNames = []
summonerId = []

for page in range(1,nb_page+1):
    res = requete("https://euw1.api.riotgames.com/lol/league/v4/entries/RANKED_SOLO_5x5/" + ELO[0] + "/" + ELO[1] + "?page=" + str(page) + "&api_key=" + KEY)
    if res.status_code != 200:
        print("error " + str(res.status_code) + " -> CHANGE THE API KEY maybe")
        exit()
    resJSON = json.loads(res.text)

    for i in range(nb_player):
        if i/205 == 1:
            break
        summonerNames.append(resJSON[i]["summonerName"])
        summonerId.append(resJSON[i]["summonerId"])
        
accountIds = []
for i in range(len(summonerNames)):
    if i%100 == 0:
        print("accountIds :" + str(i))
    res2 = requete("https://euw1.api.riotgames.com/lol/summoner/v4/summoners/" + summonerId[i] + "?api_key=" + KEY)
    res2JSON = json.loads(res2.text)
    accountIds.append(res2JSON["accountId"])

history = []
for i in range(len(accountIds)):
    if i%100 == 0:
        print("history :" + str(i))
    res3 = requete("https://euw1.api.riotgames.com/lol/match/v4/matchlists/by-account/" + accountIds[i] + "?api_key=" + KEY)
    res3JSON = json.loads(res3.text)
    history.append([])
    index = 0
    while len(history[i]) < history_size and index < res3JSON["endIndex"]:
        if res3JSON["matches"][index]["role"] == "SOLO":
            history[i].insert(0, res3JSON["matches"][index]["gameId"])
        index += 1

try:
    os.mkdir("data/" + ELO[0] + "_" + ELO[1])
except OSError:
    pass

for p in range(len(summonerNames)):
    print("joueur : " + str(p+1) + "/" + str(len(summonerNames)))
    try:
        os.mkdir("data/" + ELO[0] + "_" + ELO[1] + "/" + summonerNames[p])
    except OSError:
        pass
    
    #print(history[p])
    for i in history[p]:
        #--------- game ---------
        res4 = requete("https://euw1.api.riotgames.com/lol/match/v4/matches/"+ str(i) + "?api_key=" + KEY)
        if res4.status_code == 404 :
            print("BREAK -> error 404")
            break
        res4JSON = json.loads(res4.text) #parse le str en JSON  
        try:
            os.mkdir("data/" + ELO[0] + "_" + ELO[1] + "/" + summonerNames[p] + "/game_" + str(i))
        except OSError:
            pass
        
        critere1 = ["win", "firstBlood", "firstTower", "firstInhibitor", "firstBaron", "firstDragon", "firstRiftHerald", "towerKills", "inhibitorKills", "baronKills", "dragonKills", "riftHeraldKills"]
        data = []
        for c in critere1:
            data.append({"name" : c, "team0" : res4JSON['teams'][0][c], "team1" : res4JSON['teams'][1][c]})
        #print("Data :\n", data)

        # conversion en dataframe pandas
        dataframe = pd.DataFrame(data)
        #print("Dataframe :\n", dataframe)
        # sauvegarde (en csv, par exemple)
        dataframe.to_csv("data/"+  ELO[0] + "_" + ELO[1] + "/" + summonerNames[p] +"/game_" + str(i) + "/game.csv", index=False)

        #--------- timeline ---------
        res5 = requete("https://euw1.api.riotgames.com/lol/match/v4/timelines/by-match/"+ str(i) + "?api_key=" + KEY)
        if res5.status_code == 404 :
            print("BREAK -> error 404")
            continue
        res5JSON = json.loads(res5.text) #parse le str en JSON
        
        if len(res5JSON['frames'][0]['participantFrames']) != 10:
            print("BREAKKKKKKKKKKKKKKk")
            continue
        print("gameid :" + str(i) + "−>" + str(len(res5JSON['frames'])) + "−>" + str(len(res5JSON['frames'][0]['participantFrames'])))
        
        data = []
        critere2 = ["GOLD", "CURRENTGOLD", "LEVEL", "MINIONSKILLED", "KILLS", "BUY", "SOLD", "UNDO"]
        nb_data = len(critere2)
        data.append({})
        data.append({})
        data[0]["name"] = "nameStamp"
        data[0]["frame_0"] = "Tstart"
        data[1]["name"] = "timestamp"
        nameStamp = [0]
        for participant in range(10):
            for s in range(nb_data):
                data.append({})
                data[(participant+(participant*nb_data) + (2+s)) - participant]["name"] = "participant_" + str(participant) + "_" + critere2[s]

            
        for participant in range(10):
            kills = 0
            for f in range(len(res5JSON['frames'])):
                #print(str(i) + " " + str(participant))
                data[(participant+(participant*nb_data) + 2) - participant]["frame_" + str(f)] = res5JSON['frames'][f]["participantFrames"][str(participant+1)]["totalGold"]
                data[(participant+(participant*nb_data) + 3) - participant]["frame_" + str(f)] = res5JSON['frames'][f]["participantFrames"][str(participant+1)]["currentGold"]
                data[(participant+(participant*nb_data) + 4) - participant]["frame_" + str(f)] = res5JSON['frames'][f]["participantFrames"][str(participant+1)]["level"]
                data[(participant+(participant*nb_data) + 5) - participant]["frame_" + str(f)] = res5JSON['frames'][f]["participantFrames"][str(participant+1)]["minionsKilled"]
                data[(participant+(participant*nb_data) + 6) - participant]["frame_" + str(f)] = 0
                data[(participant+(participant*nb_data) + 7) - participant]["frame_" + str(f)] = []
                data[(participant+(participant*nb_data) + 8) - participant]["frame_" + str(f)] = []
                data[(participant+(participant*nb_data) + 9) - participant]["frame_" + str(f)] = []
                
                for event in res5JSON['frames'][f]['events']:
                    if event['type'] == "CHAMPION_KILL" and event['killerId'] == participant+1:
                        kills = kills+1
                    if (event["type"] == "ITEM_SOLD"  or event["type"] == "ITEM_DESTROYED") and event["participantId"] == participant+1:
                        data[(participant+(participant*nb_data) + 8) - participant]["frame_" + str(f)].append(event["itemId"])
                    elif event["type"] == "ITEM_UNDO" and event["participantId"] == participant+1:
                        data[(participant+(participant*nb_data) + 9) - participant]["frame_" + str(f)].append([event["afterId"], event["beforeId"]])
                    elif (event["type"] == "ITEM_PURCHASED") and event["participantId"] == participant+1:
                        data[(participant+(participant*nb_data) + 7) - participant]["frame_" + str(f)].append(event["itemId"])
        
                    if len(nameStamp) == 1 and event["type"] == "BUILDING_KILL":
                        nameStamp.append(f)
                    elif len(nameStamp) == 2 and event["type"] == "ELITE_MONSTER_KILL":
                        if event["monsterType"] == "BARON_NASHOR":
                            nameStamp.append(f)

                data[(participant+(participant*nb_data) + 6) - participant]["frame_" + str(f)] = kills
        
        teams_critere = ["KILLS", "CURRENTGOLD", "TOTALGOLD", "LEVEL", "SBIRES"]
        for c in teams_critere:
            data.append({})
            data[len(data)-1]["name"] = "team0_" + c
        for c in teams_critere:
            data.append({})
            data[len(data)-1]["name"] = "team1_" + c
        for c in teams_critere:
            data.append({})
            data[len(data)-1]["name"] = "delta_" + c

        for f in range(len(res5JSON['frames'])):
            if f != 0:
                if f <= nameStamp[1]:
                    data[0]["frame_" + str(f)] = "Tearly"
                elif len(nameStamp) < 3:
                    data[0]["frame_" + str(f)] = "Tmid"
                else:
                    if f <= nameStamp[2]:
                        data[0]["frame_" + str(f)] = "Tmid"
                    else:
                        data[0]["frame_" + str(f)] = "Tend"
            data[1]["frame_" + str(f)] = res5JSON['frames'][f]["timestamp"]

            data[len(data)-15]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 6) - participant]["frame_" + str(f)] for participant in range(5)], 0)
            data[len(data)-14]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 3) - participant]["frame_" + str(f)] for participant in range(5)], 0)
            data[len(data)-13]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 2) - participant]["frame_" + str(f)] for participant in range(5)], 0)
            data[len(data)-12]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 4) - participant]["frame_" + str(f)] for participant in range(5)], 0)
            data[len(data)-11]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 5) - participant]["frame_" + str(f)] for participant in range(5)], 0)
            data[len(data)-10]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 6) - participant]["frame_" + str(f)] for participant in range(5, 10)], 0)
            data[len(data)-9]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 3) - participant]["frame_" + str(f)] for participant in range(5, 10)], 0)
            data[len(data)-8]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 2) - participant]["frame_" + str(f)] for participant in range(5, 10)], 0)
            data[len(data)-7]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 4) - participant]["frame_" + str(f)] for participant in range(5, 10)], 0)
            data[len(data)-6]["frame_" + str(f)] = sum([data[(participant+(participant*nb_data) + 5) - participant]["frame_" + str(f)] for participant in range(5, 10)], 0)

            data[len(data)-5]["frame_" + str(f)] = data[len(data)-15]["frame_" + str(f)] - data[len(data)-10]["frame_" + str(f)]
            data[len(data)-4]["frame_" + str(f)] = data[len(data)-14]["frame_" + str(f)] - data[len(data)-9]["frame_" + str(f)]
            data[len(data)-3]["frame_" + str(f)] = data[len(data)-13]["frame_" + str(f)] - data[len(data)-8]["frame_" + str(f)]
            data[len(data)-2]["frame_" + str(f)] = data[len(data)-12]["frame_" + str(f)] - data[len(data)-7]["frame_" + str(f)]
            data[len(data)-1]["frame_" + str(f)] = data[len(data)-11]["frame_" + str(f)] - data[len(data)-6]["frame_" + str(f)]

        #print(nameStamp)
        

        #print("Data :\n", data)

        # conversion en dataframe pandas
        dataframe = pd.DataFrame(data)
        #print("Dataframe :\n", dataframe)
        
        dataframe.to_csv("data/"+  ELO[0] + "_" + ELO[1] + "/" + summonerNames[p] +"/game_" + str(i) + "/timeline.csv", index=False)
        
        #print("Dataframe :\n", dataframe)
        

for i in range(len(summonerNames)):
    print(summonerNames[i] + " -> " + accountIds[i])
    for j in range(len(history[i])):
        print("    " + str(history[i][j]))